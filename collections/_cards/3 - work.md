---
layout: card
title: 'Certificazioni'
image:
  file: 'apple-desk-laptop-working.jpg'
  alt: 'Work'
media:
  file: 'man.jpg'
  alt: 'Davide Pennica'
twitter:
  link: 'http://twitter.com/DavidePennica'
  account: '@DavidePennica'
author:
  title: 'Founder & CTO'
---

Preparati per conseguire gli attestati rilasciati dalle migliori università del mondo.

Segui i [percorsi](/corsi) che preferisci con preparazione in lingua italiana
e consegui il prestigioso attestato della [Harvard University](https://cs50.harvard.edu/college/){:target="\_blank"} in [Introduction to Computer Science](https://www.edx.org/course/cs50s-introduction-computer-science-harvardx-cs50x){:target="\_blank"}
con la piattaforma [edX](https://www.edx.org){:target="\_blank"}
