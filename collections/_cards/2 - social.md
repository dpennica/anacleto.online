---
layout: card
title: 'Design'
image:
  file: 'pexels-photo-social1.jpeg'
  alt: 'Social'
media:
  file: 'mysisia.jpeg'
  alt: 'MySisia'
twitter:
  link: 'http://twitter.com/mysisia'
  account: '@mysisia'
author:
  title: 'Creative'
---

The soul of the Beast seemed lost forever. Then, by the full
moon's light, a child was born; child with the unbridled soul of
the Beast that would make all others pale in comparison.
