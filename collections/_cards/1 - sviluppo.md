---
layout: card
title: 'Investi in te stesso'
image:
  file: 'pexels-photo-success.jpg'
  alt: 'Back to school'
media:
  file: 'man.jpg'
  alt: 'Davide Pennica'
twitter:
  link: 'http://twitter.com/DavidePennica'
  account: '@DavidePennica'
author:
  title: 'Founder & CTO'
---

L'investimento migliore che tu possa fare è su te stesso e
sul tuo futuro.

Scegli il [percorso](/corsi) che più ti interessa, completalo e avrai a
disposizione gli strumenti che ti porteranno al successo.
