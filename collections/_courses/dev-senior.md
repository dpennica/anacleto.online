---
layout: course
title: 'Percorso Programmazione Senior'
items:
  - 'Go'
  - 'Php'
  - 'Ci/Cd'
  - 'Javascript'
image:
  file: 'pexels-photo-success.jpg'
  alt: 'Back to school'
media:
  img: 'man.jpg'
  alt: 'Davide Pennica'
twitter:
  link: 'http://twitter.com/DavidePennica'
  account: '@DavidePennica'
author:
  title: 'Founder & CTO'
---

Irure eiusmod fugiat ut voluptate in duis nisi enim enim ex. Adipisicing mollit occaecat exercitation magna dolor nisi occaecat. Sint dolor deserunt ullamco occaecat sint elit irure ea sunt enim.

Amet amet ipsum elit exercitation. Consectetur reprehenderit ea cillum occaecat eu eu cillum dolore. Ullamco enim ea cupidatat non. Exercitation quis esse ut ut ex. Quis officia magna adipisicing occaecat in officia incididunt proident eiusmod enim exercitation esse sint consequat.

Aliqua laboris eiusmod adipisicing adipisicing dolore sunt. Veniam consequat esse non ipsum sint cillum eu. Velit dolor nulla sunt adipisicing nulla qui dolor consequat elit occaecat. Tempor ex commodo nostrud et. Tempor consectetur Lorem aute voluptate aliquip nulla officia nostrud nostrud aliquip anim voluptate ea.

Officia ex fugiat labore sunt. Ex excepteur anim commodo consequat duis duis adipisicing elit id do proident ipsum. Nisi amet dolore proident culpa ad dolore dolor sint. Ullamco exercitation pariatur ullamco cupidatat ex in adipisicing consequat et. Fugiat magna reprehenderit sunt pariatur ipsum aliquip et qui veniam qui. Veniam Lorem exercitation sunt irure id amet incididunt non commodo minim id labore. Laborum minim nostrud commodo mollit ut.
