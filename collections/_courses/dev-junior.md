---
layout: course
title: 'Programmazione Junior'
items:
  - 'Sistema numerico binario'
  - 'Sistema numerico esadecimale'
  - 'Sistema numerico decimale'
  - 'Porte logiche'
  - 'Algoritmi'
  - 'ReactJsStrutture dati'
  - 'Diagrammi di flusso'
  - 'Iterazioni'
  - 'Git base'
  - 'PHP'
  - 'Bash'
  - 'Ruby'
  - 'C'
image:
  file: 'pexels-photo-success.jpg'
  alt: 'Back to school'
media:
  img: 'man.jpg'
  alt: 'Davide Pennica'
twitter:
  link: 'http://twitter.com/DavidePennica'
  account: '@DavidePennica'
author:
  title: 'Founder & CTO'
---

Programma che introduce alla programmazione
