---
layout: course
title: 'Percorso Programmazione Mid'
items:
  - 'C'
  - 'C++'
  - 'Php'
  - 'Python'
  - 'Git'
  - 'JavaScript'
image:
  file: 'pexels-photo-success.jpg'
  alt: 'Back to school'
media:
  img: 'man.jpg'
  alt: 'Davide Pennica'
twitter:
  link: 'http://twitter.com/DavidePennica'
  account: '@DavidePennica'
author:
  title: 'Founder & CTO'
---

Secondo livello del percorso di programmazione
